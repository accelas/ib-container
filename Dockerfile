FROM		ubuntu:14.04
MAINTAINER	Kai Wang <i@havetowork.today>

RUN		apt-get update
RUN		apt-get install -y xvfb gsettings-desktop-schemas openjdk-7-jre && rm -rf /var/lib/apt/lists/*

ADD		.	  /src

CMD		cd /src && ./run.sh
